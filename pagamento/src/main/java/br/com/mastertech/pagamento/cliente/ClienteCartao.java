package br.com.mastertech.pagamento.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE")
public interface ClienteCartao {

    @GetMapping("/cliente/{id}")
    Cliente getById(@PathVariable Integer id);

}
