package br.com.mastertech.pagamento.controller;

import br.com.mastertech.pagamento.cliente.ClienteCartao;
import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.services.PagamentoService;
import br.com.mastertech.pagamento.services.ClienteNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class PagamentoControler {

    @Autowired
    private ClienteCartao clienteCartao;

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento cadastrarPagamento(@RequestBody @Valid Pagamento pagamento) {

        try {
            clienteCartao.getById(pagamento.getClienteId());
            Pagamento objetoPagamento = pagamentoService.salvarPagamento(pagamento);
            return objetoPagamento;
        } catch (RuntimeException e) {
            throw new ClienteNotFoundException();
        }

    }

}
