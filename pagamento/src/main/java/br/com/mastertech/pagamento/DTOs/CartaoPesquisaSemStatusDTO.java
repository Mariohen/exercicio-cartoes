package br.com.mastertech.pagamento.DTOs;

import br.com.mastertech.pagamento.model.Pagamento;

public class CartaoPesquisaSemStatusDTO {

        private Integer id;

        private String numero;

        private Integer clienteId;


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public Integer getClienteId() {
            return clienteId;
        }

        public void setClienteId(Integer clienteId) {
            this.clienteId = clienteId;
        }

        public CartaoPesquisaSemStatusDTO() {}

        public CartaoPesquisaSemStatusDTO(Pagamento pagamento) {
            this.setClienteId(pagamento.getClienteId());
            this.setId(pagamento.getId());
            this.setNumero(pagamento.getNumero());
        }

}
