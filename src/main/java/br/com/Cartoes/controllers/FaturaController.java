package br.com.Cartoes.controllers;

import br.com.Cartoes.DTOs.CartaoSaidaDTO;
import br.com.Cartoes.DTOs.CartaoStatusDTO;
import br.com.Cartoes.Models.Pagamento;
import br.com.Cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private PagamentoService pagamentoService;

    @GetMapping("/{clienteId}/{cartaoId}")
    public List<Pagamento> alterarStatusAtivo(@PathVariable(name = "clienteId") Integer clienteId,
                                              @PathVariable(name = "cartaoId") Integer cartaoId){

        try {
            return pagamentoService.buscarFatura(clienteId, cartaoId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}