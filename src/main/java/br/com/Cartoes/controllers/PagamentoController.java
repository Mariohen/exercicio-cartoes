package br.com.Cartoes.controllers;

import br.com.Cartoes.Models.Pagamento;
import br.com.Cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

//
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento cadastrarPagamento(@RequestBody @Valid Pagamento pagamento) {
        Pagamento objetoPagamento = pagamentoService.salvarPagamento(pagamento); ;
        return objetoPagamento;

    }

    @GetMapping("/{id}")
    public List<Pagamento> pesquisaPorCartaoId(@RequestParam(name = "cartaoId") int cartaoId){
        return pagamentoService.pesquisarPorCartaoID(cartaoId);
    }


}
