package br.com.Cartoes.controllers;

import br.com.Cartoes.DTOs.CartaoPesquisaSemStatusDTO;
import br.com.Cartoes.DTOs.CartaoSaidaDTO;
import br.com.Cartoes.DTOs.CartaoStatusDTO;
import br.com.Cartoes.Models.Cartao;
import br.com.Cartoes.Models.Cliente;
import br.com.Cartoes.services.CartaoService;
import br.com.Cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao cadastrarCartao(@RequestBody @Valid Cartao cartao) {
        Cartao objetoCartao = cartaoService.salvarCartao(cartao);
        return objetoCartao;

    }

    @PatchMapping("/{numero}")
    public CartaoSaidaDTO alterarStatusAtivo(@PathVariable(name = "numero") String numero, @RequestBody @Valid CartaoStatusDTO cartaoStatusDTO)
    {
        try
        {
            return cartaoService.alterarStatusAtivo(numero, cartaoStatusDTO);
        }
        catch (RuntimeException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @GetMapping("/{numero}")
    public CartaoPesquisaSemStatusDTO buscarPorNumero(@PathVariable(name = "numero") String numero){
        try {
            return cartaoService.buscarPorNumero(numero);
        }
        catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
