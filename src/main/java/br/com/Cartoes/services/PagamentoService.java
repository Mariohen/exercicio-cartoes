package br.com.Cartoes.services;

import br.com.Cartoes.Models.Pagamento;
import br.com.Cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento){
        cartaoService.buscarCartaoPeloId(pagamento.getCartao_Id());
        if (cartaoService != null){
            Pagamento objetoPagamento = pagamentoRepository.save(pagamento);
            return objetoPagamento;
        } else {
            throw new RuntimeException("O cartao não foi encontrado");
        }
     }


    //retorno de todos os registros
    public Iterable<Pagamento> lerTodosOsProdutos(){
        return pagamentoRepository.findAll();
    }


    public List<Pagamento> pesquisarPorCartaoID(int cartaoId){
        return pagamentoRepository.findByCartaoId(cartaoId);

    }

    public List<Pagamento> buscarFatura(Integer clienteId, Integer cartaoId){
        cartaoService.buscarCartaoPeloId(cartaoId);
        if (cartaoService != null){
                clienteService.buscarClientePorId(clienteId);
                if (clienteService != null){
                    return pagamentoRepository.findByCartaoId(cartaoId);
                } else {
                    throw new RuntimeException("O cliente não foi encontrado");
                }

        } else {
            throw new RuntimeException("O cartao não foi encontrado");
        }
    }




}
