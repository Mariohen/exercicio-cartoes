package br.com.Cartoes.services;

import br.com.Cartoes.DTOs.CartaoPesquisaSemStatusDTO;
import br.com.Cartoes.DTOs.CartaoSaidaDTO;
import br.com.Cartoes.DTOs.CartaoStatusDTO;
import br.com.Cartoes.Models.Cartao;
import br.com.Cartoes.Models.Cliente;
import br.com.Cartoes.Models.Pagamento;
import br.com.Cartoes.repositories.CartaoRepository;
import br.com.Cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

//    @Autowired
//    private CartaoPesquisaSemStatusDTO cartaoPesquisaSemStatusDTO;

    @Autowired
    private ClienteService clienteService;

    public Cartao salvarCartao(Cartao cartao) {

        Optional<Cliente> cliente = clienteRepository.findById(cartao.getClienteId());
        if (cliente.isPresent()) {
//          cartao.setIdCliente(clienteId);
            Cartao objetoCartao = cartaoRepository.save(cartao);
            return cartao;
        } else {
            throw new RuntimeException("Cliente não localizado");

        }
    }

    public Cartao buscarCartaoPeloNumero(String numero) {
        Cartao cartao = cartaoRepository.findFirstByNumero(numero);

        if (cartao != null) {
            return cartao;
        } else {
            throw new RuntimeException("O cartao não foi encontrado");
        }
    }

    public Cartao buscarCartaoPeloId(int id) {
        Cartao cartao = cartaoRepository.findFirstById(id);

        if (cartao != null) {
            return cartao;
        } else {
            throw new RuntimeException("O cartao não foi encontrado");
        }
    }

    public CartaoSaidaDTO alterarStatusAtivo(String numero, CartaoStatusDTO cartaoStatusDTO){
        Cartao cartao = cartaoRepository.findFirstByNumero(numero);
        if(cartao != null){
            cartao.setAtivo(cartaoStatusDTO.getAtivo());
            cartao = cartaoRepository.save(cartao);

            return new CartaoSaidaDTO(cartao);
        }
        else
        {
            throw new RuntimeException("Número do cartão não existe");
        }
    }


    public CartaoPesquisaSemStatusDTO buscarPorNumero(String numero){

        Cartao cartao = cartaoRepository.findFirstByNumero(numero);
        if(cartao != null){
            return new CartaoPesquisaSemStatusDTO(cartao);
        }
        else {
            throw new RuntimeException("Número de cartão não existe");
        }
    }



}
