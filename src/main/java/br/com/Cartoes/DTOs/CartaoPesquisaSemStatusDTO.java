package br.com.Cartoes.DTOs;

import br.com.Cartoes.Models.Cartao;


public class CartaoPesquisaSemStatusDTO {
        private Integer id;

        private String numero;

        private Integer clienteId;


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public Integer getClienteId() {
            return clienteId;
        }

        public void setClienteId(Integer clienteId) {
            this.clienteId = clienteId;
        }

        public CartaoPesquisaSemStatusDTO() {}

        public CartaoPesquisaSemStatusDTO(Cartao cartao) {
            this.setClienteId(cartao.getClienteId());
            this.setId(cartao.getId());
            this.setNumero(cartao.getNumero());
        }

}

