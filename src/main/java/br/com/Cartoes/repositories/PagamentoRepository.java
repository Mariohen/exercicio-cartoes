package br.com.Cartoes.repositories;

import br.com.Cartoes.Models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository <Pagamento, Integer> {

    List<Pagamento> findByCartaoId(int cartaoId);

}
