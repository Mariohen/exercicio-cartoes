package br.com.Cartoes.repositories;

import br.com.Cartoes.Models.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

      Cartao findFirstByNumero(String numero);
      Cartao findFirstById(int id);

}
