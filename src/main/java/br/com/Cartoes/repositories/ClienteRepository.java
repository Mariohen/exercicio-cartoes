package br.com.Cartoes.repositories;

import br.com.Cartoes.Models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

 //   Cliente findFirstById(Integer id);

}
