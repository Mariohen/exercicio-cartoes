package br.com.Cliente.controllers;

import br.com.Cliente.models.Cliente;
import br.com.Cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente) {
        Cliente objetoCliente = clienteService.salvarCliente(cliente);
        return objetoCliente;

    }

    @GetMapping("/{id}")
    public Cliente pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarClientePorId(id);
            return cliente;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}